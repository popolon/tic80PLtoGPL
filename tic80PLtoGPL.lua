#!/usr/bin/lua

-- tool to generate Tic-80 palettes to Gimp Palette (GPL) Format
-- License: WTFWYW
-- Popolon <popolon@popolon.org>

local PALETTES =
{
        {name="DB16",data="140c1c44243430346d4e4a4e854c30346524d04648757161597dced27d2c8595a16daa2cd2aa996dc2cadad45edeeed6"},
        {name="PICO-8",data="0000007e25531d2b535f574fab5236008751ff004d83769cff77a8ffa300c2c3c700e756ffccaa29adfffff024fff1e8"},
        {name="ARNE16",data="0000001b2632005784493c2ba4642244891abe26332f484e31a2f2eb89319d9d9da3ce27e06f8bb2dceff7e26bffffff"},
        {name="EDG16",data="193d3f3f2832743f399e2835b86f50327345e53b444f67810484d1fb922bafbfd263c64de4a6722ce8f4ffe762ffffff"},
        {name="A64",data="0000004c3435313a9148545492562b509450b148638080787655a28385cf9cabb19ccc47cd93738fbfd5bbc840ede6c8"},
        {name="C64",data="00000057420040318d5050508b542955a0498839327878788b3f967869c49f9f9f94e089b8696267b6bdbfce72ffffff"},
        {name="VIC20",data="000000772d2642348ba85fb4b668627e70caa8734a559e4ae99df5e9b287bdcc7185d4dc92df87c5ffffffffb0ffffff"},
        {name="CGA",data="000000aa00000000aa555555aa550000aa00ff5555aaaaaa5555ffaa00aa00aaaa55ff55ff55ff55ffffffff55ffffff"},
        {name="SLIFE",data="0000001226153f28117a2222513155d13b27286fb85d853acc8218e07f8a9b8bff68c127c7b581b3e868a8e4d4ffffff"},
        {name="JMP",data="000000191028833129453e78216c4bdc534b7664fed365c846af45e18d79afaab9d6b97b9ec2e8a1d685e9d8a1f5f4eb"},
        {name="CGARNE",data="0000002234d15c2e788a36225e606e0c7e45e23d69aa5c3d4c81fb44aacceb8a60b5b5b56cd9477be2f9ffd93fffffff"},
        {name="PSYG",data="0000001b1e29003308362747084a3c443f41a2324e52524c546a0073615064647c516cbf77785be08b799ea4a7cbe8f7"},
        {name="EROGE",data="0d080d2a23494f2b247d384032535f825b314180a0c16c5bc591547bb24e74adbbe89973bebbb2f0bd77fbdf9bfff9e4"},
        {name="EISLAND",data="051625794765686086567864ca657e8686918184abcc8d867ea78839d4b98dbcd29dc085edc38de6d1d1f5e17af6f6bf"},
}

local index=1

-- print("number of palettes:"..#PALETTES)
-- Scan list
for index=1,#PALETTES,1 do
  -- Choose palette in the list
  pal = PALETTES[index].data
  nam = PALETTES[index].name

  -- print ("palette: "..nam)
  -- Open file in update mode
  fh = io.open("tic80-"..nam..".gpl","w+")
  io.output(fh)
  --print GPL format header
  io.write("GIMP Palette\n")
  io.write("Name: tic80-"..nam.."\n")
  io.write("#\n")

  --for each color, convert format, then print a line
  for i=1,#pal,6 do
  --  print tonumber(pal:sub(i,i+1),16))
    R = string.sub(pal, i,i+1)
    R2 = string.format("%3s",tonumber(R,16))
    G = string.sub(pal, i+2,i+3)
    G2 = string.format("%3s",tonumber(G,16))
    B = string.sub(pal, i+4,i+5)
    B2 = string.format("%3s",tonumber(B,16))
    --  print ("sub["..string.format("%2s",i).."] = R:0x"..R.." ("..R2.."), G:0x"..G.."( "..G2.."), B:0x"..B.." ("..B2..")")
    io.write (R2.." "..G2.." "..B2.."\n")
  end
  io.close(fh)
end
