tool to create GIMP Palette (.gpl) format from tic-80 (http://tic.computer) internal palette format.

This palette format, is used by at least :
* Gimp http://gimp.org/
* Inkscape (vector drawing)a http://inkscape.org/
* MyPaint (bitmap drawing with nice brushes) http://mypaint.org/
* Krita (bitmap drawing, also using MyPaint brush system) http://krita.org/
* Agave (Colorscheme designer tool) http://web.archive.org/web/20170308183221/http://home.gna.org/colorscheme/

simply execute it, it will extract data to one GPL file by palette, with tic80- prefix and .gpl suffix


* Using generated palettes with Gimp

To use it with GIMP, push the .gpl file generated in ~/.gimp-2.8/palettes/

or in /usr/share/gimp/2.0/palettes/ for a wider system usage.

Tip: you will probably need to replace 2.8 and/or 2.0 depending on your gimp version.

Other tools sometimes read this directory, else you could search in the application documentation or around the web.


* Futur

If there are more palette, it could be updated to read automagically Tic-80 sources ?
